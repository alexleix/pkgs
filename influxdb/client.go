package influxdb

import influxdb2 "github.com/influxdata/influxdb-client-go/v2"

//针对influxdb的客户端使用
var client influxdb2.Client

//添加ssl options
//初始化
func InitInfluxDb(token, bucket, org, host string) influxdb2.Client {
	client = influxdb2.NewClient(host, token)
	return client
}

//需要用户关闭
func Close() {
	client.Close()
}

//写入数据
func WriteData(pointName string, data interface{}) {

}

//读取数据
