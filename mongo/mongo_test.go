package mongo

import (
	"testing"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	DefaultClientName = "default-mongo"
	DBName            = "imooc"
	TableName         = "test"
)

var mongoClient *MgClient

type User struct {
	ID   int64  `bson:"id"`
	Name string `bson:"name"`
}

//自定义_id
type UserWithID struct {
	ID   int64  `bson:"_id"` //设定mongo自带的_id
	Name string `bson:"name"`
}

//默认的mongo生成的_id
type UserWithPrimitiveID struct {
	ID   primitive.ObjectID `bson:"_id"`
	Name string             `bson:"name"`
}

//测试初始化操作
func init() {
	InitMongoClient(DefaultClientName, "admin", "admin1234", []string{"127.0.0.1:27017"}, 100)
	mongoClient = GetMongoClient(DefaultClientName)
}

//插入多条数据
func TestInsertManyDocs(t *testing.T) {
	users1 := User{
		ID:   1,
		Name: "test1",
	}
	users2 := User{
		ID:   2,
		Name: "test2",
	}
	err := mongoClient.InsertMany(DBName, TableName, users1, users2)
	if err != nil {
		t.Logf("insertMany error:%+v", err)
	}
}

func TestInsertManyTryBest(t *testing.T) {
	users1 := UserWithID{
		ID:   3,
		Name: "test1",
	}
	err := mongoClient.InsertManyTryBest(DBName, TableName, users1)
	if err != nil {
		t.Logf("insertMany error:%+v", err)
	}
}

func TestUpsert(t *testing.T) {
	update := map[string]interface{}{"Name": "test333"}
	filter := bson.M{"$set": update}
	t.Log(filter)
	err := mongoClient.Upsert(DBName, TableName, bson.D{{"_id", 3}}, update)
	if err != nil {
		t.Logf("Upsert error:%+v", err)
	}
}

//替换数据
func TestReplaceOne(t *testing.T) {
	err := mongoClient.ReplaceOne(DBName, TableName, bson.D{{"_id", 3}}, UserWithID{
		ID:   3,
		Name: "test768",
	})
	if err != nil {
		t.Logf("Upsert error:%+v", err)
	}
}

//单个更新
func TestUpdateOne(t *testing.T) {
	update := map[string]interface{}{"name": "test88888"}

	err := mongoClient.UpdateOne(DBName, TableName, bson.D{{"_id", 3}}, update)
	if err != nil {
		t.Logf("Upsert error:%+v", err)
	}
}

//多个更新
func TestUpdateMany(t *testing.T) {
	update := bson.M{
		"$set": bson.M{"name": "test88778123"},
	}
	err := mongoClient.UpdateMany(DBName, TableName, bson.D{{"name", "test1"}}, update)
	if err != nil {
		t.Logf("Upsert error:%+v", err)
	}
}
