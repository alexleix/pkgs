package timeutil

import (
	"math"
	"net/http"
	"strconv"
	"time"
)

var (
	cst *time.Location
)

//CSTLayout China standard Time Layout
const CSTLayout = "2006-01-02 15:04:05"
const YMDLayout = "20060102"

func init() {
	var err error
	if cst, err = time.LoadLocation("Asia/Shanghai"); err != nil {
		panic(err)
	}
	//设置默认时区
	time.Local = cst
}

//RFC3339ToCSTLayout convert rfc3339 value to china standard time layout
func RFC3339ToCSTLayout(value string) (string, error) {
	ts, err := time.Parse(time.RFC3339, value)
	if err != nil {
		return "", err
	}
	return ts.In(cst).Format(CSTLayout), nil
}

// CSTLayoutString 格式化时间
// 返回 "2006-01-02 15:04:05" 格式的时间
func CSTLayoutString() string {
	ts := time.Now()
	return ts.In(cst).Format(CSTLayout)
}

// ParseCSTInLocation 格式化时间
func ParseCSTInLocation(date string) (time.Time, error) {
	return time.ParseInLocation(CSTLayout, date, cst)
}

//返回unxi时间戳
func CSTLayoutStringToUnix(cstLayoutString string) (int64, error) {
	stamp, err := time.ParseInLocation(CSTLayout, cstLayoutString, cst)
	if err != nil {
		return 0, err
	}
	return stamp.Unix(), nil
}

// GMTLayoutString 格式化时间
// 返回 "Mon, 02 Jan 2006 15:04:05 GMT" 格式的时间
//Tue, 29 Nov 2022 16:31:18 GMT
func GMTLayoutString() string {
	return time.Now().In(cst).Format(http.TimeFormat)
}

// ParseGMTInLocation 格式化时间
func ParseGMTInLocation(date string) (time.Time, error) {
	return time.ParseInLocation(http.TimeFormat, date, cst)
}

//20221129
func YMDLayoutInt64(t time.Time) int64 {
	ymdTime, _ := strconv.ParseInt(t.In(cst).Format(YMDLayout), 10, 64)
	return ymdTime
}

func YMDLayoutString(t time.Time) string {
	return t.In(cst).Format(YMDLayout)
}

//SubInLocation 对比当前时间计算差 返回正值 秒为单位
func SubInLocation(ts time.Time) float64 {
	return math.Abs(float64(time.Now().In(cst).Sub(ts).Seconds()))
}
