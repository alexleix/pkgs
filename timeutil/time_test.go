package timeutil

import (
	"testing"
	"time"
)

func TestRFC3339ToCSTLayout(t *testing.T) {
	date, _ := RFC3339ToCSTLayout("2022-11-29 16:30:08")
	t.Log(date)
}

func TestCSTLayoutString(t *testing.T) {
	t.Log(CSTLayoutString())
}

func TestCSTLayoutStringToUnix(t *testing.T) {
	t.Log(CSTLayoutStringToUnix("2022-11-29 16:30:08"))
}

func TestGMTLayoutString(t *testing.T) {
	t.Log(GMTLayoutString())
}

func TestYMDLayoutInt64(t *testing.T) {
	var time_d = time.Now()
	tm := YMDLayoutInt64(time_d)
	t.Log(tm)
}

//
func TestSubInLocation(t *testing.T) {
	time_before, _ := ParseCSTInLocation("2022-11-29 16:31:08")
	sub_time := SubInLocation(time_before)
	t.Log(sub_time)
}
