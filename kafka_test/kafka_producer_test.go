package kafka_producer_test

import (
	"encoding/json"
	"reflect"
	"testing"
	"time"

	"gitee.com/alexleix/pkgs/mq"
	"github.com/Shopify/sarama"
)

var (
	hosts = []string{"127.0.0.1:9092"}
	topic = "test-topic"
)

type Msg struct {
	ID        int64  `json:"id"`
	Name      string `json:"name"`
	CreatedAt int64  `json:"created_at"`
}

//同步的sync kafka请求 默认测试
func TestKafkaSyncProducer(t *testing.T) {
	//1.初始化DefaultKafkaSyncProducer 并且归入map中
	err := mq.InitSyncKafkaProducer(mq.DefaultKafkaSyncProducer, hosts, nil)
	if err != nil {
		t.Logf("InitSyncKafkaProducer error:%+v", err)
	}
	msg := Msg{
		ID:        1,
		Name:      "test name sync",
		CreatedAt: time.Now().Unix(),
	}
	msgBody, _ := json.Marshal(msg)

	//获取sync 同步的需求
	part, offset, err := mq.GetKafkaSyncProducer(mq.DefaultKafkaSyncProducer).Send(&sarama.ProducerMessage{
		Topic: topic,
		//序列化成byte
		Value: mq.KafkaMsgValueEncoder(msgBody),
	})
	t.Logf("the partition is %d", part)
	t.Logf("the offset is %d", offset)
	if err != nil {
		t.Logf("Send msg error:%+v", err)
	} else {
		t.Log("Send msg success")
	}
}

// === RUN   TestKafkaSyncProducer
// SyncKafkaProducer connected namedefault-kafka-sync-producer
//     /Users/zhulei/go/src/pkgs/kafka_test/kafka_producer_test.go:42: the partition is 0
//     /Users/zhulei/go/src/pkgs/kafka_test/kafka_producer_test.go:43: the offset is 3
// Send msg success
// --- PASS: TestKafkaSyncProducer (0.01s)

//异步测试
func TestKafkaASync(t *testing.T) {
	//1. 默认配置初始化异步客户端
	err := mq.InitAsyncKafkaProducer(mq.DefaultKafkaAsyncProducer, hosts, nil)
	if err != nil {
		t.Logf("InitAsyncKafkaProducer error:%+v", err)
	}
	msg := Msg{
		ID:        1,
		Name:      "test name Async",
		CreatedAt: time.Now().Unix(),
	}
	msgBody, _ := json.Marshal(msg)
	err = mq.GetKafkaASyncProducer(mq.DefaultKafkaAsyncProducer).Send(&sarama.ProducerMessage{
		Topic: topic,
		//序列化成字符串
		Value: mq.KafkaMsgValueStrEncoder(msgBody),
	})
	if err != nil {
		t.Logf("Send async msg error:%+v", err)
	} else {
		t.Log("Send async msg success")
	}
	time.Sleep(3 * time.Second) //异步等待提交完成
}

//消费测试
func TestKafkaConsumer(t *testing.T) {
	//初始化并开启消费者
	_, err := mq.StartKafkaConsumer(hosts, []string{topic}, "kafka-test", nil, func(message *sarama.ConsumerMessage) (bool, error) {
		//目标want
		var msg_want = &Msg{
			ID:        1,
			Name:      "test name sync",
			CreatedAt: 1668486751,
		}
		t.Log("消费消息:", "topic:", message.Topic, "Partition:", message.Partition, "Offset:", message.Offset, "value:", string(message.Value))
		msg := &Msg{}
		err := json.Unmarshal(message.Value, msg)
		if err != nil {
			t.Logf("the message unmarshal err:%+v", err)
			return true, nil
		}
		//判断是否相同
		ismirror := reflect.DeepEqual(msg_want, msg)
		t.Log(ismirror)
		return true, nil
	})
	if err != nil {
		t.Logf("the consumer start err:%+v", err)
	}
	// select {
	// case s := <-signals:
	// 	mq.KafkaStdLogger.Println("kafka test receive system signal:", s)
	// 	return
	// }
}
