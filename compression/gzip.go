package compression

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
)

func GzipEncode(input []byte) ([]byte, error) {
	//创建一个新的byte输出流
	var buf bytes.Buffer
	//创建一个新的gzip输出流
	//NoCompression      = flate.NoCompression      // 不压缩
	//BestSpeed          = flate.BestSpeed          // 最快速度
	//BestCompression    = flate.BestCompression    // 最佳压缩比
	//DefaultCompression = flate.DefaultCompression // 默认压缩比
	//gzip.NewWriterLevel()
	gzipWriter := gzip.NewWriter(&buf)
	//将input byte 数组写入到此输出流中
	_, err := gzipWriter.Write(input)
	if err != nil {
		_ = gzipWriter.Close()
		return nil, err
	}
	if err := gzipWriter.Close(); err != nil {
		return nil, err
	}
	//返回压缩后的 bytes数组
	return buf.Bytes(), nil
}

func GzipDecode(input []byte) ([]byte, error) {
	//创建一个新的gzip.Reader
	bytesReader := bytes.NewReader(input) //读取字节
	gzipReader, err := gzip.NewReader(bytesReader)
	if err != nil {
		return nil, err
	}
	defer func() {
		//defer 中关闭gzipReader
		_ = gzipReader.Close()
	}()
	buf := new(bytes.Buffer)
	//从Reader中读取数据
	if _, err := buf.ReadFrom(gzipReader); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

//压缩
func MarshalJsonAndGzip(data interface{}) ([]byte, error) {
	//转成 []byte
	marshalData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	//压缩
	gzipData, err := GzipEncode(marshalData)
	if err != nil {
		return nil, err
	}
	return gzipData, nil
}

//解压
func UnmarshalDataFromJsonWithGzip(input []byte, output interface{}) (err error) {
	decodeData, err := GzipDecode(input)
	if err != nil {
		return err
	}
	err = json.Unmarshal(decodeData, output)
	if err != nil {
		return err
	}
	return nil
}
