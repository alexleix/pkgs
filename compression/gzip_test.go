package compression

import "testing"

type User struct {
	Id   int64
	Name string
	Age  uint8
}

var user_info = User{
	Id:   1,
	Name: "testing",
	Age:  29,
}

func TestMarshalJsonAndGzip(t *testing.T) {
	data, err := MarshalJsonAndGzip(user_info)
	if err != nil {
		t.Errorf("the MarshalJsonAndGzip error:%+v", err)
	}
	t.Logf(string(data))
}

func TestUnmarshalDataFromJsonWithGzip(t *testing.T) {
	data, err := MarshalJsonAndGzip(user_info)
	if err != nil {
		t.Errorf("the MarshalJsonAndGzip error:%+v", err)
	}
	var UserTest = User{}
	err = UnmarshalDataFromJsonWithGzip(data, &UserTest)
	if err != nil {
		t.Errorf("UnmarshalDataFromJsonWithGzip err %v", err)
	}
	t.Log("outputUser : ", UserTest)
}
